#!/usr/bin/env bash

lxsession &
picom &
/usr/bin/emacs --daemon &
#nm-applet &
nitrogen --restore &
xrandr -s 1920x1200 &
dunst &
#sxhkd &
#conky -c $HOME/.config/conky/doomone-qtile.conkyrc
